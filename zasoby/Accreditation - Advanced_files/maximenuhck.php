
.clr {clear:both;}

/*-------------------------
---	 global styles		---
--------------------------*/

/* for z-index layout */
div#maximenuck {

}

/* container style */
div#maximenuck ul.maximenuck {
    background :  url(../images/fond_bg.png) top left repeat-x;
    height : 34px;
    padding : 0;
    margin : 0;
    overflow: visible !important;
	display: block !important;
	float: none !important;
	visibility: visible !important;
}

div#maximenuck ul.maximenuck li.maximenuck {
    background : none;
    list-style : none;
    border : none;
}

/* link image style */
div#maximenuck ul.maximenuck li.maximenuck > a img {
    margin : 3px;
    border : none;
}

/* img style without link (in separator) */
div#maximenuck ul.maximenuck li.maximenuck img {
    border : none;
	clear: both;
}

div#maximenuck ul.maximenuck li a.maximenuck,
div#maximenuck ul.maximenuck li span.separator {
    text-decoration : none;
    text-indent : 2px;
	min-height : 34px;
    outline : none;
    background : none;
    border : none;
    padding : 0;
    color : #ccc;
    white-space: normal;
}


/*-------------------------
---	 active items		---
--------------------------*/

/* active parent title */
div#maximenuck ul.maximenuck li.active > a span.titreck {
    color : #ccc;
}

/* active parent description */
div#maximenuck ul.maximenuck li.active > a span.descck {

}

/* active parent title */
div#maximenuck ul.maximenuck li.active > a:hover span.titreck {
    color : #fff;
}

/*-----------------------------
---	 1st level items		---
------------------------------*/

div#maximenuck ul.maximenuck li.level1 {
    padding : 0 10px;
    background : url(../images/separator.png) top right no-repeat;
}

/* first level item title */
div#maximenuck ul.maximenuck li.level1 > a span.titreck,
div#maximenuck ul.maximenuck li.level1 > span.separator span.titreck {
    color : #ccc;
}

/* first level item description */
div#maximenuck ul.maximenuck li.level1 > a span.descck {
    color : #ccc;
}

/* first level item link */
div#maximenuck ul.maximenuck li.parent.level1 > a,
div#maximenuck ul.maximenuck li.parent.level1 > span {
/* 	background : url(../images/maxi_arrow0.png) bottom right no-repeat; */
}

/* parent style level 0 */
div#maximenuck ul.maximenuck li.parent.level1 li.parent {
    background : url(../images/maxi_arrow1.png) center right no-repeat;
}

/* first level item hovered */
div#maximenuck ul.maximenuck li.level1>a:hover span.titreck,
div#maximenuck ul.maximenuck li.level1>span:hover span.titreck {
    color: #fff;
}


/**
** items title and descriptions
**/

/* item title */
div#maximenuck span.titreck {
    color : #888;
    /*display : block;*/
    text-transform : none;
    font-weight : normal;
    font-size : 11px;
    line-height : 34px;
    text-decoration : none;
	/*height : 17px;*/
    min-height :34px;
    float : none !important;
    float : left;
-webkit-font-smoothing: antialiased;    
}

/* item description */
div#maximenuck span.descck {
    color : #c0c0c0;
    display : block;
    text-transform : none;
    font-size : 10px;
    text-decoration : none;
    height : 12px;
    line-height : 12px;
    float : none !important;
    float : left;
-webkit-font-smoothing: antialiased;
}

/* item title when mouseover */
div#maximenuck ul.maximenuck  a:hover span.titreck {
    color : #ddd;
}

/**
** child items
**/

/* child item title */
div#maximenuck ul.maximenuck2 li a.maximenuck,
div#maximenuck ul.maximenuck2 li span.separator {
    text-decoration : none;
    border-bottom : 1px solid #505050;
    margin : 0 2%;
	width: 96%;
    padding : 3px 0 3px 0;
	clear:both;
}

/* child item block */
div#maximenuck ul.maximenuck ul.maximenuck2 {
    background : transparent;
    margin : 0;
    padding : 0;
    border : none;
    width : 100%; /* important for Chrome and Safari compatibility */
    position: static;
}

div#maximenuck ul.maximenuck2 li.maximenuck {
    padding : 2px 0 0 0;
    border : none;
    margin : 0 5px;
    background : none;
    display : block;
    float: none;
	/*clear:both;*/
}

/* child item container  */
div#maximenuck ul.maximenuck li div.floatck {
    background : #1a1a1a;
    border : 1px solid #707070;
}

/**
** module style
**/

div#maximenuck div.maximenuck_mod {
    width : 100%;
    padding : 0;
    /*overflow : hidden;*/
    color : #ddd;
    white-space : normal;
}

div#maximenuck div.maximenuck_mod div.moduletable {
    border : none;
    background : none;
}

div#maximenuck div.maximenuck_mod  fieldset{
    width : 100%;
    padding : 0;
    margin : 0 auto;
    /*overflow : hidden;*/
    background : transparent;
    border : none;
}

div#maximenuck ul.maximenuck2 div.maximenuck_mod a {
    border : none;
    margin : 0;
    padding : 0;
    display : inline;
    background : transparent;
    color : #888;
    font-weight : normal;
}

div#maximenuck ul.maximenuck2 div.maximenuck_mod a:hover {
    color : #FFF;
}

/* module title */
div#maximenuck ul.maximenuck div.maximenuck_mod h3 {
    font-size : 14px;
    width : 100%;
    color : #aaa;
    font-size : 14px;
    font-weight : normal;
    background : #444;
    margin : 5px 0 0 0;
    padding : 3px 0 3px 0;
}

div#maximenuck ul.maximenuck2 div.maximenuck_mod ul {
    margin : 0;
    padding : 0;
    width : 100%;
    background : none;
    border : none;
    text-align : left;
}

div#maximenuck ul.maximenuck2 div.maximenuck_mod li {
    margin : 0 0 0 15px;
    padding : 0;
    width : 100%;
    background : none;
    border : none;
    text-align : left;
    font-size : 11px;
    float : none;
    display : block;
    line-height : 20px;
    white-space : normal;
}

/* login module */
div#maximenuck ul.maximenuck2 div.maximenuck_mod #form-login ul {
    left : 0;
    margin : 0;
    padding : 0;
    width : 100%;
}

div#maximenuck ul.maximenuck2 div.maximenuck_mod #form-login ul li {
    margin : 2px 0;
    padding : 0 5px;
    height : 20px;
    background : transparent;
}


/**
** columns width & child position
**/

/* child blocks position (from level2 to n) */
div#maximenuck ul.maximenuck li.maximenuck div.floatck div.floatck {
    margin : -30px 0 0 180px;
}

/* margin for overflown elements that rolls to the left */
div#maximenuck ul.maximenuck li.maximenuck div.floatck div.floatck.fixRight  {
    margin-right : 180px;
}

/* default width */
div#maximenuck ul.maximenuck li div.floatck {
    width : 180px;
}

div#maximenuck ul.maximenuck li div.floatck div.maximenuck2 {
    width : 180px;
}

/* 2 cols width */
div#maximenuck ul.maximenuck li div.cols2 {
    width : 360px;
}

div#maximenuck ul.maximenuck li div.cols2>div.maximenuck2 {
    width : 50%;
}

/* 3 cols width */
div#maximenuck ul.maximenuck li div.cols3 {
    width : 540px;
}

div#maximenuck ul.maximenuck li div.cols3>div.maximenuck2 {
    width : 33%;
}

/* 4 cols width */
div#maximenuck ul.maximenuck li div.cols4 {
    width : 720px;
}

div#maximenuck ul.maximenuck li div.cols4>div.maximenuck2 {
    width : 25%;
}



/**
** fancy parameters
**/

div#maximenuck .maxiFancybackground {
    list-style : none;
    padding: 0 !important;
    margin: 0 !important;
    border: none !important;
}

div#maximenuck .maxiFancybackground .maxiFancycenter {
    background: url('../images/fancy_bg.png') repeat-x top left;
    height : 34px;
}

div#maximenuck .maxiFancybackground .maxiFancyleft {

}

div#maximenuck .maxiFancybackground .maxiFancyright {

}

/**
** rounded style
**/

/* global container */
div#maximenuck div.maxiRoundedleft {

}

div#maximenuck div.maxiRoundedcenter {

}

div#maximenuck div.maxiRoundedright {

}

/* child container */
div#maximenuck div.maxidrop-top {

}

div#maximenuck div.maxidrop-main {

}

div#maximenuck div.maxidrop-bottom {

}


/* bouton to close on click */
div#maximenuck span.maxiclose {
    color: #fff;
}

/*** compatibility layer for native template Beez20 and Beez5 ***/
#header
{
    overflow:visible !important;
	z-index:1000 !important;
}

#header ul.menu li a:link,
#header ul.menu li a:visited
{
	display: block !important;
}

div#maximenuck ul.menu li ul li ul li ul
{
	border:none !important;
	padding-top:0px !important;
	padding-bottom:0px !important;
	background:transparent !important;
}

div#maximenuck ul.menu li ul li ul
{
	border:0 !important;
	background:transparent !important;
	padding-bottom:0;
}

#header ul.menu li.maximenuck.active a:link,
#header ul.menu li.maximenuck.active a:visited
{
border-right:none !important;
background:transparent !important;
}

#header ul.menu
{
margin-top:0px !important;
}