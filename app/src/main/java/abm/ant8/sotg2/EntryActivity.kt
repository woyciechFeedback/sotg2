package abm.ant8.sotg2

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import org.jetbrains.anko.frameLayout

class EntryActivity : AppCompatActivity() {
    val frameLayoutID = 102

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        frameLayout {
            id = frameLayoutID
        }
        supportFragmentManager.beginTransaction().add(frameLayoutID, EntryActivityFragment(), "EntryFragmentStackEntry").commit()
    }

}
