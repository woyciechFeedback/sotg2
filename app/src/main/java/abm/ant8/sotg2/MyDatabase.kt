package abm.ant8.sotg2

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteCursorDriver
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteQuery
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper

/**
 * Created by Ant8 on 2016-06-14.
 */
class MyDatabase(context: Context?, name: String? = "questions_db.db", storageDirectory : String, version: Int = 1) : SQLiteAssetHelper(context, name, storageDirectory, null, version) {

}