package abm.ant8.sotg2

import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.VideoView
import org.jetbrains.anko.*
import org.jetbrains.anko.db.IntParser
import org.jetbrains.anko.db.rowParser
import org.jetbrains.anko.db.select
import java.io.File
import java.util.*

class MainActivity : AppCompatActivity(), AnkoLogger {
//      needed to be accessible everywhere (possibly injected some time later)
    var button1Correct: Boolean = false;
    var question: Question = Question()
    var recentlyAskedQuestions: MutableList<Int> = mutableListOf()
    var categoriesUsed: MutableSet<Int> = mutableSetOf()
    var answers: MutableMap<Int, Boolean> = mutableMapOf()
    var level: Int = 0
    var mode: Int = 0
    var totalNoOfQuestions = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bundle = this.intent.extras
        level = bundle.getInt("level")?:0
        mode = bundle.getInt("mode")?:0

        var mTextView = TextView(this)
        var button2 = Button(this)
        var button1 = Button(this)
//        var mVideoView = VideoView(this)

//         kept for future reference
//        val defaultSharedPreferences = defaultSharedPreferences
//        val sharedPrefsEditor = defaultSharedPreferences.edit()

//        val booleanKey = "Boolowska"

//        sharedPrefsEditor.putBoolean(booleanKey, false).apply()

//        getting database up, get total no of questions by chance
        var myDatabase = MyDatabase(context = this, storageDirectory = this.applicationContext.getExternalFilesDir(null).absolutePath)

        var sqliteDatabase = myDatabase.readableDatabase

        totalNoOfQuestions = sqliteDatabase
                .select("questions")
                .where("level_id <= {lookedUpLevel}",
                        "lookedUpLevel" to level)
                .parseList(rowParser(parseToQuestion()))
                .size

//        creating layout
        verticalLayout {
            padding = dip(16)
            mTextView = textView("") {
                textSize = 20f
            }
//            mVideoView = videoView {
//            }
            button1 = button("") {
                textSize = 20f
                onClick { if (mode == 1) {
                        handleAnswer(1, button1, button2, mTextView, sqliteDatabase)
                    } else {
                        handleAnswer(1, button1, button2, mTextView, sqliteDatabase)
                    }
                }
            }
            button2 = button("") {
                textSize = 20f
                onClick { if (mode == 1) {
                        handleAnswer(2, button1, button2, mTextView, sqliteDatabase)
                    } else {
                        handleAnswer(2, button1, button2, mTextView, sqliteDatabase)
                    }
                }
            }
        }

//        retrieve state of activity when rotating or start from scratch otherwise
        recentlyAskedQuestions = if (savedInstanceState?.get("recentlyAskedQuestions") != null) savedInstanceState?.get("recentlyAskedQuestions") as MutableList<Int> else mutableListOf()

        if (recentlyAskedQuestions.isNotEmpty()) {
            fetchQuestion(button1, button2, mTextView, sqliteDatabase, recentlyAskedQuestions?.last())
        } else {
            fetchQuestion(button1, button2, mTextView, sqliteDatabase)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putIntegerArrayList("recentlyAskedQuestions", recentlyAskedQuestions as ArrayList<Int>?)
    }

    private fun handleAnswer(contextButton: Int, button1: Button, button2: Button, mTextView: TextView, sqliteDatabase: SQLiteDatabase) {
        val isAnswerCorrect = (contextButton == 1 && button1.text == question.correctAnswer) || (contextButton == 2 && button2.text == question.correctAnswer)

        if (mode == 2) {
            Log.d("TAGG", "rozmiar listy odp przed ${answers.size}")
//            debug("rozmiar listy odp przed ${answers.size}")
            answers.put(answers.size, isAnswerCorrect)
            Log.d("TAGG", "rozmiar listy odp po ${answers.size}")
//            debug("rozmiar listy odp po ${answers.size}")
            if (answers.size < 7) {
                fetchQuestion(button1, button2, mTextView, sqliteDatabase)
            } else {
                testSummaryAlert()
            }
        }

        if (mode == 1) {
            var message = if (isAnswerCorrect) "Dobrze" else "Źle"
            alert(question.explanation, message) {
                yesButton {
                        fetchQuestion(button1, button2, mTextView, sqliteDatabase)
                }
            }.show()
        }
    }

    private fun testSummaryAlert() {
        var totalCorrect = 0
        for ((k, v) in answers) {
            if (v) totalCorrect++
        }
        alert("Poprawnych odpowiedzi $totalCorrect na ${answers.size}", "Podsumowanie testu") {
            yesButton { finish() }
        }.show()
    }

    private fun fetchQuestion(button1: Button, button2: Button, mTextView: TextView, sqliteDatabase: SQLiteDatabase, restoredQuestion: Int = 0) {
        doAsync {
            val rand = Random()
            var questionNumber: Int

            if (restoredQuestion == 0) {
                do {
                    questionNumber = rand.nextInt((totalNoOfQuestions - 1) + 1) + 1
                } while (questionNumber in recentlyAskedQuestions)
//                @todo move it up to a whole quiz constant and unify it with test length
                val minIntervalBetweenSameQuestion = if (mode == 1) 2 else 9


                if (recentlyAskedQuestions.size > minIntervalBetweenSameQuestion) recentlyAskedQuestions.remove(recentlyAskedQuestions.first())

                recentlyAskedQuestions.add(questionNumber)

            } else {
                questionNumber = restoredQuestion
            }

            question = sqliteDatabase
                    .select("questions")
                    .where("(id = {lookedUpId}) and (level_id <= {lookedUpLevel})",
                            "lookedUpId" to questionNumber,
                            "lookedUpLevel" to level)
                    .parseSingle(rowParser(parseToQuestion()))

            uiThread {
                mTextView.text = question.question
                if (rand.nextInt() % 2 == 0) {
                    button1Correct = true
                    button1.text = question.correctAnswer
                    button2.text = question.wrongAnswer
                } else {
                    button1Correct = false
                    button1.text = question.wrongAnswer
                    button2.text = question.correctAnswer
                }
            }
        }
    }

    private fun parseToQuestion(): (Int, String, String, String, Int, Int, String) -> Question {
        return { id: Int, question: String, correctAnswer: String, wrongAnswer: String, level_id: Int, category: Int, explanation: String ->
            Question(id, question, correctAnswer, wrongAnswer, level_id, category, explanation)
        }
    }

}
