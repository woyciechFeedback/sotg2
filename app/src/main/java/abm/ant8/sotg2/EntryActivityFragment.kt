package abm.ant8.sotg2

import android.support.v4.app.Fragment
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
//import org.jetbrains.anko.UI
import org.jetbrains.anko.*

/**
 * A placeholder fragment containing a simple view.
 */
class EntryActivityFragment : Fragment() {
    var mode: Int = -1
    var level = -1
    var checkedLevel = -1
    var checkedMode = -1


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var levelRadioGroup: RadioGroup = RadioGroup(this.context)
        var modeRadioGroup: RadioGroup = RadioGroup(this.context)
        var view = with(AnkoContext.create(this.context, this)) {
            verticalLayout {
                padding = dip(16)
                verticalLayout {
                    val mTextView = textView("wybierz poziom") {
                        textSize = 20f
                    }
                    levelRadioGroup = radioGroup {
                        radioButton {
                            id = 1
                            text = "podstawowy"
                            textSize = 20f
                        }
                        radioButton {
                            id = 2
                            text = "zaawansowany"
                            textSize = 20f
                        }
                        onCheckedChange {
                            radioGroup, i -> level = i
                        }
                    }
                }
                verticalLayout {
                    val mTextView = textView("wybierz tryb") {
                        textSize = 20f
                    }
                    modeRadioGroup = radioGroup {
                        radioButton {
                            id = 1
                            text = "nauka"
                            textSize = 20f
                        }
                        radioButton {
                            id = 2
                            text = "test"
                            textSize = 20f
                        }
                        onCheckedChange {
                            radioGroup, i -> mode = i
                        }
                    }
                }

                val buttonGo = button("DALEJ") {
                    textSize = 20f
                    onClick {
                        if (mode == -1)
                            toast("wybierz tryb")
                        else if (level == -1)
                            toast("wybierz poziom")
                        else
                            startActivity<MainActivity>("level" to level, "mode" to mode)
                    }
                }
            }
        }

        return view
    }
}

