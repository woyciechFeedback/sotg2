package abm.ant8.sotg2

/**
 * Created by Ant8 on 2016-06-15.
 */
class Question(val id: Int = 0,
               val question: String = "",
               val correctAnswer: String = "",
               val wrongAnswer: String = "",
               val level_id: Int = 0,
               val category: Int = 0,
               val explanation: String = "") {
}