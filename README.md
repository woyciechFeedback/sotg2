# README #

This app is developed as a side activity of ongoing process of my own struggle to improve as a developer as well as an ultimate player. To develop it, I used Kotlin together with SQLite (with a provided database) and Anko helper libraries.

### What is this repository for? ###

This is basically a showcase. If you want to know how the deployed app works and look like, see https://youtu.be/uJyg2vhA4wA ,

for beta version visit Google Play Store: https://play.google.com/apps/testing/abm.ant8.sotg2

Still, I want this app to be really useful and functionable in near future (say, a month or two), but I want to start small and improve gradually and steadily.